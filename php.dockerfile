FROM surnet/alpine-wkhtmltopdf:3.12.4-0.12.6-full AS builder

FROM php:7.4-fpm-alpine

ADD ./php/www.conf /usr/local/etc/php-fpm.d/www.conf

RUN addgroup -g 1000 laravel && adduser -G laravel -g laravel -s /bin/sh -D laravel

RUN mkdir -p /var/www/html

RUN chown laravel:laravel /var/www/html

WORKDIR /var/www/html

# Alpine 3.11 and higher versions have libstdc++ v9+ in their repositories which breaks the build
RUN echo 'http://dl-cdn.alpinelinux.org/alpine/v3.10/main' >> /etc/apk/repositories

RUN apk add --no-cache \
  libstdc++=8.3.0-r0 \
  libx11 \
  libxrender \
  libxext \
  libssl1.1 \
  ca-certificates \
  fontconfig \
  freetype \
  ttf-dejavu \
  ttf-droid \
  ttf-freefont \
  ttf-liberation \
  ttf-ubuntu-font-family \
&& apk add --no-cache --virtual .build-deps \
  msttcorefonts-installer \
\
# Install microsoft fonts
&& update-ms-fonts \
&& fc-cache -f \
\
# Clean up when done
&& rm -rf /tmp/* \
&& apk del .build-deps

COPY --from=builder /bin/wkhtmltopdf /bin/wkhtmltopdf
COPY --from=builder /bin/wkhtmltoimage /bin/wkhtmltoimage
COPY --from=builder /bin/libwkhtmltox* /bin/

RUN set -ex && apk --no-cache add postgresql-dev mariadb-dev sqlite-dev freetype-dev libjpeg-turbo-dev libpng-dev libzip-dev curl-dev libxml2-dev oniguruma-dev
RUN docker-php-ext-configure zip --with-zip

RUN docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/

RUN docker-php-ext-configure pgsql -with-pgsql=/usr/include/postgresql

RUN docker-php-ext-install pdo pgsql pdo_pgsql pdo_mysql pdo_sqlite gd exif opcache zip ctype curl xml fileinfo json mbstring

RUN docker-php-ext-enable gd

#COPY wkhtmltopdf /bin
#RUN chmod 777 /bin/wkhtmltopdf
